package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lambda {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat sdf_ymd = new SimpleDateFormat("yyyy-MM-dd");
	
	public interface StringToCalendar{
		Calendar convert(String d);
	}
	
	public static StringToCalendar convertor = (String d) -> {
		Date date = null;
		
		try{
			date = sdf.parse(d);
		}catch(ParseException e){
			e.printStackTrace();
		}
		
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar;
		};
	
	
	public interface LogReader{
		List<MonitoredData> read(String fileName);
	}
	
	public static LogReader logReader = (String fileName) -> {
		List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();

		//read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			List<String> result = stream.collect(Collectors.toList());
			monitoredData = result.stream()
					.map(temp->{
						String[] info = temp.split("		");
						Calendar startTime = convertor.convert(info[0]);
						Calendar endTime = convertor.convert(info[1]);
						String activityLabel = info[2];
						return new MonitoredData(startTime,endTime,activityLabel);})
					.collect(Collectors.toList());
			

		} catch (IOException e) {
			e.printStackTrace();
		}

		return monitoredData;
	};
	
	public interface GetDates{
		Set<String> compute(List<MonitoredData> monitoredData);
	}
	
	public static GetDates getDifferentDates = (monitoredData) ->{
		Set<String> dates = monitoredData.stream().map(e -> sdf_ymd.format(e.getStartTime().getTime())).collect(Collectors.toSet());
		return dates;
	};
	
	public interface Activities{
		Map<String, Long> getOccurences(List<MonitoredData> monitoredData);
	}
	
	public static Activities countOccurences = (List<MonitoredData> monitoredData) -> {
		final Map<String, Long> map = monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting()));
		try{
			Files.write(Paths.get("a2.txt"), ()->map.entrySet()
					.stream().<CharSequence>map(e -> e.getValue() + "  ->  " + e.getKey()).iterator());
					
		}catch(IOException e){
			e.printStackTrace();
		}
		return map;
	};
	
	public interface ActivitiesForEachDay{
		Map<Integer,Map<String,Long>> compute(List<MonitoredData> monitoredData);
	}

	
	public static ActivitiesForEachDay getEachDayActivities = (List<MonitoredData> monitoredData) -> {
		Map<Integer,Map<String,Long>> program = monitoredData.stream().collect(Collectors.groupingBy(
				MonitoredData::dateExtractor,Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting())));
		try{
			Files.write(Paths.get("a3.txt"), ()->program.entrySet()
					.stream().<CharSequence>map(e -> e.getKey() + "  ->  " + e.getValue()).iterator());
					
		}catch(IOException e){
			e.printStackTrace();
		}
		return program;
	};
	
	public interface DurationCalculator{
		Map<String,Long> compute(List<MonitoredData> monitoredData);
	}
	
	public static DurationCalculator durationCalculator = (monitoredData) -> {
		Map<String,Long> durations = monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel
				,Collectors.summingLong(MonitoredData::calculateDuration)));
		List<String> result = durations.entrySet().stream()
                .filter(map -> new Long(36000).compareTo(map.getValue()) == -1 )
                .map(map -> map.getKey())
                .collect(Collectors.toList());
		
		//result.forEach(System.out::println);
		try{
			Files.write(Paths.get("a4.txt"), ()->durations.entrySet()
					.stream().filter(map -> new Long(36000).compareTo(map.getValue()) == -1 )
					.<CharSequence>map(e -> {
						long seconds = e.getValue();
						long hours = seconds/3600;
						
						seconds -= hours*3600;
						long minutes = seconds/60;
						seconds -= minutes*60;
						
						return e.getKey() +" ---> " + hours + "h " + minutes + "m " + seconds + "s";
					}).iterator());
					
		}catch(IOException e){
			e.printStackTrace();
		}
		//System.out.println("-----------------------------------------");
		//durations.entrySet().forEach(e -> {
			//System.out.println(e.getKey() + " " + e.getValue());
		//});
		return durations;
	};
	
	public interface ActivitiesFilter{
		List<String> compute(List<MonitoredData> monitoredData);
	}
	
	public static ActivitiesFilter filterActivities = (monitoredData) -> {
		Map<String,Long> filteredActivites = monitoredData.stream().filter(data -> data.calculateDuration() < 300).collect(Collectors
				.groupingBy(MonitoredData::getActivityLabel,Collectors.counting()));
		Map<String,Long> activities = countOccurences.getOccurences(monitoredData);
		activities.entrySet().forEach(System.out::println);
		System.out.println("--------------------------------");
		filteredActivites.entrySet().forEach(System.out::println);
		List<String> fa = filteredActivites.entrySet().stream().filter(e -> {
			String activity = e.getKey();
			long totalOccurences = activities.entrySet().stream().filter(x -> activity.equals(x.getKey()))
					.map(temp -> temp.getValue()).findAny().orElse(null);
			float r = (float)e.getValue() / totalOccurences;
			return r > 0.7;
		}).map(temp -> temp.getKey()).collect(Collectors.toList());
		try{
			final FileWriter fw = new FileWriter("a5.txt");
			fa.forEach(activity -> {
				try {
		            fw.write(String.format("%s%n", activity));
		        } catch (IOException ex) {
		            ex.printStackTrace();
		        }
			});
			try {
	            fw.close();
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        }
		}catch(IOException e){
			e.printStackTrace();
		}
		return fa;
	};
	
}