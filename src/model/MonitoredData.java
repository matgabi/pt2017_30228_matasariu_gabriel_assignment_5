package model;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;


public class MonitoredData{
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat sdf_ymd = new SimpleDateFormat("yyyy-MM-dd");
	
	Calendar startTime;
	Calendar endTime;
	String activityLabel;
	
	public MonitoredData(Calendar startTime,Calendar endTime,String activityLabel){
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}
	
	public Calendar getStartTime(){
		return this.startTime;
	}
	
	public Calendar getEndTime(){
		return this.endTime;
	}
	
	public String getActivityLabel(){
		return this.activityLabel;
	}
	
	public String toString(){
		return sdf.format(this.startTime.getTime())+"  "+sdf.format(this.endTime.getTime())+"  "+this.activityLabel;
	}
	
	public int dateExtractor(){
		return this.getStartTime().get(Calendar.DAY_OF_MONTH);
	}
	
	public long calculateDuration(){
		
		
		return Math.abs((this.getEndTime().getTimeInMillis() - this.getStartTime().getTimeInMillis())/1000);
	}
	
	public static void main(String[] args){
		
		List<MonitoredData> monitoredData = Lambda.logReader.read("activities.txt");
		Set<String> dates = Lambda.getDifferentDates.compute(monitoredData);
		
		System.out.println("Total entries : " + monitoredData.size());
		System.out.println("Dates of interest :");
		dates.forEach(System.out::println);
		System.out.println("Total days of interest : " + dates.size());
		
		Map<String, Long> actionsInfo = Lambda.countOccurences.getOccurences(monitoredData);
		Map<Integer,Map<String,Long>> actionsForEachDay = Lambda.getEachDayActivities.compute(monitoredData);
		
		//monitoredData.forEach(e -> System.out.println(e+ " "+e.calculateDuration()));
		
	   Map<String,Long> activityDuration = Lambda.durationCalculator.compute(monitoredData);
	   List<String> act = Lambda.filterActivities.compute(monitoredData);
		
	}
}